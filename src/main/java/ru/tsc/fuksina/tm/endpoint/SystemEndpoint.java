package ru.tsc.fuksina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.fuksina.tm.api.service.IPropertyService;
import ru.tsc.fuksina.tm.api.service.IServiceLocator;
import ru.tsc.fuksina.tm.dto.request.ServerAboutRequest;
import ru.tsc.fuksina.tm.dto.request.ServerVersionRequest;
import ru.tsc.fuksina.tm.dto.response.ServerAboutResponse;
import ru.tsc.fuksina.tm.dto.response.ServerVersionResponse;

public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
