package ru.tsc.fuksina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.request.ServerAboutRequest;
import ru.tsc.fuksina.tm.dto.request.ServerVersionRequest;
import ru.tsc.fuksina.tm.dto.response.ServerAboutResponse;
import ru.tsc.fuksina.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
