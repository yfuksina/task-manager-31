package ru.tsc.fuksina.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.exception.AbstractException;

public class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Command `" + command + "` not supported...");
    }

}
